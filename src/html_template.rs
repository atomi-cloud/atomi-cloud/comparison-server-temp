pub fn html(title: &str, description: &str, author: &str, content: &str) -> String {
    format!(r#"<!doctype html>
<html lang="en">
<head>
 <meta charset="utf-8">
 <title{}</title>
 <meta name="description" content="{}">
 <meta name="author" content="{}">
 <style>
 body {{
    font-family: monospace;
    font-size: 30px;
 }}
 div {{
    display: flex;
 }}
 span {{
    display: block;
    width: 300px;
    font-weight: bold;
 }}
 </style>
</head>
<body>
 {}
</body>
</html>"#, title, description, author, content)
}