#[derive(Serialize, Deserialize, Debug)]
pub struct Continent {
    pub name: String,
    pub regions: Vec<Region>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Region {
    pub id: String,
    pub name: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Providers {
    pub providers: Vec<Provider>
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Provider {
    pub provider: String,
    pub services: Vec<Service>,
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct Service {
    pub  service: String,
    pub is_static: bool,
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct Products {
    pub products: Vec<Product>,
    pub scraping_time: String,

}

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct Product {
    pub category: String,
    #[serde(rename = "type")]
    pub tp: String,
    pub on_demand_price: f64,
    pub spot_price: Option<Vec<SpotPrice>>,
    pub cpus_per_vm: f64,
    pub mem_per_vm: f64,
    pub gpus_per_vm: f64,
    pub ntw_perf: String,
    pub ntw_perf_category: String,
    pub zones: Option<Vec<String>>,
    pub attributes: Attributes,
    pub current_gen: bool,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct SpotPrice {
    pub zone: String,
    pub price: f64,
}


#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct Attributes {
    pub cpu: String,
    pub instance_type_category: String,
    pub memory: String,
    pub network_perf_category: String,
}
