use std::collections::HashMap;

#[derive(Serialize, Deserialize, Clone)]
pub struct Compute {
    pub category: String,
    pub machine_type: String,
    pub on_demand_price: f64,
    pub cpu: f64,
    pub mem: f64,
    pub gpu: f64,
    pub network_perf: String,
    pub zones: Vec<String>,
    pub current_gen: bool,
    pub provider: String,
    pub region: String,
    pub region_name: String,
    pub continent: String,
}


pub type ContinentData = HashMap<String, ProviderRegion>;
pub type ProviderRegion = HashMap<String, HashMap<String, String>>;

#[derive(Serialize, Deserialize, Clone)]
pub struct Data {
    pub compute: Vec<Compute>,
    pub categories: Vec<String>,
    pub max_price: f64,
    pub min_price: f64,
    pub max_cpu: f64,
    pub min_cpu: f64,
    pub max_gpu: f64,
    pub min_gpu: f64,
    pub max_mem: f64,
    pub min_mem: f64,
    pub providers: Vec<String>,
    pub continents: ContinentData,
}