#![feature(proc_macro_hygiene)]
extern crate chrono;
#[macro_use]
extern crate rocket;
extern crate rocket_contrib;
#[macro_use]
extern crate serde_derive;

use std::collections::HashMap;
use std::sync::{Arc, Mutex};
use std::thread;
use std::time::Duration;

use chrono::prelude::*;
use rocket::response::content::Html;
use rocket::State;
use rocket_contrib::json::Json;

use crate::model::{Compute, ContinentData, Data};

mod html_template;
mod banzai;
mod model;
mod cors;

static mut TIME: String = String::new();
static mut LAST_UPDATE: String = String::new();

type Database = Arc<Mutex<Data>>;


#[get("/")]
fn home() -> Html<String> {
    let version = std::env::var("APP_VERSION").unwrap_or("unknown".to_string());
    let slot = std::env::var("REPLICA_SLOT").unwrap_or("unknown".to_string());
    let service = std::env::var("SERVICE_NAME").unwrap_or("unknown".to_string());
    let date = Local::now();
    let time_now = date.format("%Y-%m-%d  %H:%M:%S").to_string();
    let content = unsafe {
        format!("<div>ALL OK</div><br>\
    <div><span>Version</span>       : {}</div>\
    <div><span>Start Up Time</span> : {}</div>\
    <div><span>Time Now</span>      : {}</div>\
    <div><span>Last Update</span>   : {}</div>\
    <div><span>Service</span>       : {}</div>\
    <div><span>Replica</span>       : {}</div>", version, TIME, time_now, LAST_UPDATE, service, slot)
    };
    let h = html_template::html("System State", "State of API Server", "kirinnee", &content);
    Html(h)
}

#[derive(Serialize, Deserialize)]
struct Query {
    provider: Option<Vec<String>>,
    region: Option<Vec<String>>,
    continent: Option<Vec<String>>,
    category: Option<Vec<String>>,
    max_price: f64,
    min_price: f64,
    max_gpu: f64,
    min_gpu: f64,
    max_cpu: f64,
    min_cpu: f64,
    max_mem: f64,
    min_mem: f64,
}

#[derive(Serialize, Deserialize)]
struct Limits {
    pub categories: Vec<String>,
    pub max_price: f64,
    pub min_price: f64,
    pub max_cpu: f64,
    pub min_cpu: f64,
    pub max_gpu: f64,
    pub min_gpu: f64,
    pub max_mem: f64,
    pub min_mem: f64,
    pub providers: Vec<String>,
    pub continents: ContinentData,
}

#[get("/limits")]
fn limits(map: State<'_, Database>) -> Json<Limits> {
    let db = map.lock().unwrap();
    Json(Limits {
        categories: db.categories.clone(),
        max_price: db.max_price,
        min_price: db.min_price,
        max_cpu: db.max_cpu,
        min_cpu: db.min_cpu,
        max_gpu: db.max_gpu,
        min_gpu: db.min_gpu,
        max_mem: db.max_mem,
        min_mem: db.min_mem,
        providers: db.providers.clone(),
        continents: db.continents.clone(),
    })
}

#[options("/query")]
fn cors_fix() {}

#[post("/query", format = "json", data = "<query>")]
fn query(query: Json<Query>, map: State<'_, Database>) -> Json<Vec<Compute>> {
    let db = map.lock().unwrap();

    let r: Vec<_> = db.compute.iter()
        .filter(|x| x.cpu >= query.min_cpu && x.cpu <= query.max_cpu)
        .filter(|x| x.gpu >= query.min_gpu && x.gpu <= query.max_gpu)
        .filter(|x| x.mem >= query.min_mem && x.mem <= query.max_mem)
        .filter(|x| x.on_demand_price >= query.min_price && x.gpu <= query.max_price)
        .filter(|x| match &query.provider {
            Some(p) => p.iter().any(|a| *a == x.provider),
            None => true,
        })
        .filter(|x| match &query.region {
            Some(p) => p.iter().any(|a| *a == x.region),
            None => true,
        })
        .filter(|x| match &query.category {
            Some(p) => p.iter().any(|a| *a == x.category),
            None => true,
        })
        .filter(|x| match &query.continent {
            Some(p) => p.iter().any(|a| *a == x.continent),
            None => true,
        })
        .map(|x| x.clone()).collect();
    return Json(r);
}


fn main() {
    unsafe {
        let date = Local::now();
        let f = date.format("%Y-%m-%d  %H:%M:%S").to_string();
        TIME = f
    }

    let database = Arc::new(Mutex::new(Data {
        max_mem: 0.0,
        min_mem: 0.0,
        compute: vec![],
        categories: vec![],
        max_price: 0.0,
        min_price: 0.0,
        max_cpu: 0.0,
        min_cpu: 0.0,
        max_gpu: 0.0,
        min_gpu: 0.0,
        providers: vec![],
        continents: HashMap::new(),
    }));
    let worker = Arc::clone(&database);
    thread::spawn(move || {
        loop {
            {
                let mut db = worker.lock().unwrap();
                let c = banzai::get_all_compute().unwrap();
                *db = c;
                unsafe {
                    let date = Local::now();
                    let f = date.format("%Y-%m-%d  %H:%M:%S").to_string();
                    LAST_UPDATE = f
                }
            }
            thread::sleep(Duration::from_secs(60 * 60 * 12));
        }
    });


    let _ = rocket::ignite()
        .mount("/", routes![home,query,limits,cors_fix])
        .manage(Arc::clone(&database))
        .attach(cors::CORS())
        .launch();
}

