use std::collections::{HashMap, HashSet};

use reqwest::Error;
use serde::de::DeserializeOwned;

use crate::banzai::response::{Continent, Products, Providers};
use crate::model::{Compute, Data};

pub mod response;

pub struct Banzai {
    endpoint: String
}

impl Banzai {
    pub fn new() -> Banzai {
        Banzai { endpoint: "https://banzaicloud.com/cloudinfo/api/v1".to_string() }
    }

    fn get<T>(&self, s: &str) -> Result<T, Error> where T: DeserializeOwned {
        let request_url = format!("{}/{}", self.endpoint, s);
        println!("URL : {}", request_url);
//        let r = reqwest::blocking::get(&request_url)?;
//        let t = r.text()?;
//        println!("URL : {}", t);
        let r = reqwest::blocking::get(&request_url)?;
        let response = r.json()?;
        Ok(response)
    }


    pub fn providers(&self) -> Result<Providers, Error> {
        self.get("providers")
    }


    pub fn continents(&self, provider: &str) -> Result<Vec<Continent>, Error> {
        let url = format!("providers/{}/services/compute/continents", provider);
        self.get(&url)
    }

    pub fn servers(&self, provider: &str, region: &str) -> Result<Products, Error> {
        let url = format!("providers/{}/services/compute/regions/{}/products", provider, region);
        self.get(&url)
    }
}

pub fn get_all_compute() -> Result<Data, Error> {
    let b = Banzai::new();
    let providers = b.providers()?;


    let mut pvds = HashSet::<String>::new();
    let mut c_data = HashMap::new();

    let mut cats = HashSet::<String>::new();
    let mut computes: Vec<Compute> = vec![];

    let mut max_price = -1.0f64;
    let mut min_price = std::f64::MAX;
    let mut max_cpu = -1.0f64;
    let mut min_cpu = std::f64::MAX;
    let mut max_gpu = -1.0f64;
    let mut min_gpu = std::f64::MAX;
    let mut max_mem = -1.0f64;
    let mut min_mem = std::f64::MAX;


    for p in providers.providers {
        let pp = p.provider.clone();
        // Inject providers
        if !pvds.contains(&pp) {
            pvds.insert(p.provider.clone());
        }

        let continents = b.continents(&pp)?;
        for c in continents {
            let mut con_name = c.name.clone();
            let regions = c.regions;
            for r in regions {
                if r.id == "norwayest" || r.id == "switzerlandnorth" || r.id == "germanywestcentral" {
                    con_name = String::from("Europe")
                }
                if r.id == "uaenorth" {
                    con_name = String::from("Asia")
                }

                let c_data_c = c_data.entry(con_name.clone()).or_insert(HashMap::new());
                let r_datas = c_data_c.entry(pp.clone()).or_insert(HashMap::new());
                if !r_datas.contains_key(&r.id) {
                    r_datas.insert(r.id.clone(), r.name.clone());
                }


                let compute = b.servers(&p.provider, &r.id)?;
                for com in compute.products {
                    if !cats.contains(&com.category) {
                        cats.insert(com.category.clone());
                    }

                    max_price = max_price.max(com.on_demand_price);
                    min_price = min_price.min(com.on_demand_price);
                    max_cpu = com.cpus_per_vm.max(max_cpu);
                    min_cpu = min_cpu.min(com.cpus_per_vm);
                    max_gpu = max_gpu.max(com.gpus_per_vm);
                    min_gpu = min_gpu.min(com.gpus_per_vm);
                    max_mem = max_mem.max(com.mem_per_vm);
                    min_mem = min_mem.min(com.mem_per_vm);

                    let c = Compute {
                        category: com.category,
                        machine_type: com.tp,
                        on_demand_price: com.on_demand_price,
                        cpu: com.cpus_per_vm,
                        mem: com.mem_per_vm,
                        gpu: com.gpus_per_vm,
                        network_perf: com.ntw_perf,
                        zones: com.zones.unwrap_or(vec![]),
                        current_gen: com.current_gen,
                        provider: p.provider.clone(),
                        region: r.id.clone(),
                        region_name: r.name.clone(),
                        continent: con_name.clone(),
                    };

                    computes.push(c);
                }
            }
        }
    }

    let data = Data {
        compute: computes,
        categories: cats.into_iter().collect(),
        max_price,
        min_price,
        max_cpu,
        min_cpu,
        max_gpu,
        min_gpu,
        max_mem,
        min_mem,
        providers: pvds.into_iter().collect(),
        continents: c_data,
    };

    Ok(data)
}